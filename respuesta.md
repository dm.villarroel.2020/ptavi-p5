
Ejercicio 3. Análisis general

¿Cuántos paquetes componen la captura?
Tiene 1050 paquetes

¿Cuánto tiempo dura la captura?
10 minutos 52 segundos

¿Qué IP tiene la máquina donde se ha efectuado la captura?
192.168.1.116

¿Se trata de una IP pública o de una IP privada?
Es una IP privada

¿Por qué lo sabes?
Porque está dentro del rango 192.168.0.0 a 192.168.255.255

¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
SIP UDP RTP

¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
ICM, Ethernet

¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
RTP con un tráfico de 134kilobytes/s

¿En qué segundos tienen lugar los dos primeros envíos SIP?
en t = 0 s y t= 0.663 s

Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
En el 6 con t = 0.17485 s

Los paquetes RTP, ¿cada cuánto se envían?
Cada 0.01 s

Ejercicio 4. Primeras tramas

¿De qué protocolo de nivel de aplicación son?
SIP

¿Cuál es la dirección IP de la máquina "Linphone"?
192.168.1.116

¿Cuál es la dirección IP de la máquina "Servidor"?
212.79.111.155

¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
Envía un INVITE para establecer la conexion con usuario music@sip.iptel.org

¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
Envia un mensaje 100 trying a Linphone para abrir y establecer la comunicación

¿De qué protocolo de nivel de aplicación son?
SIP

¿Entre qué máquinas se envía cada trama?
Servidor, Linphone y viceversa

¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
El servidor envia un 200 ok, se establece la conexión

¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
Linphone ha enviado el ACK al 200 ok para notificar de la llegada

Ejercicio 5. Tramas finales

¿Qué número de trama es?
1042

¿De qué máquina a qué máquina va?
De la máquina 192.168.1.116 a la máquina 212.79.111.155 

¿Para qué sirve?
Para finalizar la comunicación 

¿Puedes localizar en ella qué versión de Linphone se está usando?
User-Agent: Linphone Desktop/4.3.2 (Debian GNU/Linux bookworm/sid, Qt 5.15.6) LinphoneCore/5.0.37

Ejercicio 6. Invitación

¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
music@sip.iptel.org

¿Qué instrucciones SIP entiende el UA?
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE

¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
Content-Type: application/sdp

¿Cuál es el nombre de la sesión SIP?
Session Name (s): Talk

Ejercicio 7. Indicación de comienzo de conversación

¿Qué trama lleva esta propuesta?
La 1

¿Qué indica el 7078?
El puerto que utilizará Linphone para recibir los los paquetes de datos 

¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
Que se enviaran a ese destino

¿Qué paquetes son esos?
RTP

¿Qué trama lleva esta respuesta?
4

¿Qué valor es el XXX?
29448

¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
Que se enviaran a ese destino

¿Qué paquetes son esos?
RTP

Ejercicio 8. Primeros paquetes RTP

¿De qué máquina a qué máquina va?
De la máquina 192.168.1.116 a la máquina 212.79.111.155. De Linphone a Servidor

¿Qué tipo de datos transporta?
Multimedia

¿Qué tamaño tiene?
214 bytes

¿Cuántos bits van en la "carga de pago" (payload)
179 bytes

¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
0.01 s

Ejercicio 9. Flujos RTP

¿Cuántos flujos hay? ¿por qué?
Hay 2 flujos, linphone y servidor

¿Cuántos paquetes se pierden?
Ninguno

Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
30.726700 ms

¿Qué es lo que significa el valor de delta?
La delta es la diferencia de tiempo entre envio de paquetes

¿En qué flujo son mayores los valores de jitter (medio y máximo)?
En el del servidor

¿Qué significan esos valores?
La variacion del retardo

¿Cuánto valen el delta y el jitter para ese paquete?
La delta vale 0.000164 ms, y el jitter = 3,087 ms

¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
Si podemos, en el skew

El "skew" es negativo, ¿qué quiere decir eso?
Que llegó antes, 4.527 ms

¿Qué se oye al pulsar play?
Una canción

¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
Se escuchan cortes y más lenta

¿A qué se debe la diferencia?
Al cambio del buffer
